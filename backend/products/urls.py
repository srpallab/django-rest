from django.urls import path
from .import views

app_name = 'products'

urlpatterns = [
    path('', views.ProductListCreateAPIView.as_view()),
    path('update/<int:pk>/', views.ProductUpdateAPIView.as_view()),
    path('delete/<int:pk>/', views.ProductDestroyAPIView.as_view()),
    path('<int:pk>/', views.ProductDetailsAPIView.as_view()),
]

from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField(null=True, blank=True)
    price = models.DecimalField(
        max_digits=15, decimal_places=3, default=00.000)

    @property
    def sale_price(self):
        return "%.2f" % (float(self.price) * 0.8)

    def discount(self):
        return "%.2f" % (float(self.price) * 0.2)

    def __str__(self):
        return self.title

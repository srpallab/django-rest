from rest_framework import generics
from api.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import Product
from .serializers import ProductSerializer


class ProductListCreateAPIView(generics.ListCreateAPIView):
    """
    DRF Generic Product Create End Point
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    # To add user on data save
    def preform_create(self, serializer):
        serializer.save(user=self.request.user)


class ProductDetailsAPIView(generics.RetrieveAPIView):
    """
    DRF Generic Product List End Point
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductUpdateAPIView(generics.UpdateAPIView):
    """
    DRF Generic Product Update End Point
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'pk'


class ProductDestroyAPIView(generics.DestroyAPIView):
    """
    DRF Generic Product Delete End Point
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'pk'

# from rest_framework.decorators import api_view
# from rest_framework.response import Response

# from products.models import Product
# from products.serializers import ProductSerializer


# @api_view(['GET'])
# def api_home(request, *args, **kwargs):
#     """
#     DRF API View
#     """
#     data = {}
#     instance = Product.objects.all().order_by("?").first()
#     if instance:
#         data = ProductSerializer(instance).data
#     return Response(data)


# @api_view(['POST'])
# def add_product(request, *args, **kwargs):
#     """
#     DRF API Add Product
#     """
#     serializer_data = ProductSerializer(data=request.data)
#     if serializer_data.is_valid(raise_exception=True):
#         instance = serializer_data.save()
#         print(instance)
#         return Response(serializer_data.data)
#     return Response({"Error": "Someting Went Wrong"})

import requests

endpoint = "http://localhost:8000/api/product/update/3/"

data = {
    "title": "motor gorilla",
    "content": "Updated from Clinet",
    "price": 449.99
}

get_response = requests.put(endpoint, json=data)

print(get_response.status_code)
print(get_response.json())

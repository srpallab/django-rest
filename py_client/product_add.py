import requests

endpoint = "http://localhost:8000/api/product/"

data = {"title": "From Client Generic",
        "content": "Testing From Client", "price": 99.99}

get_response = requests.post(endpoint, json=data)

print(get_response.status_code)
print(get_response.json())

import requests

product_id = input("Product ID to delete")

try:
    product_id = int(product_id)
except:
    product_id = None
    print(f'{product_id} is no valid id')


if product_id:
    endpoint = f'http://localhost:8000/api/product/delete/{product_id}/'
    get_response = requests.delete(endpoint)
    print(get_response.status_code)

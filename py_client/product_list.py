import requests
from getpass import getpass

username = input("Enter Your Username: ")
password = getpass("Enter Your Password: ")

auth_endpoint = "http://localhost:8000/api/auth/"
auth_data = {
    "username": username,
    "password": password
}

auth_response = requests.post(auth_endpoint, json=auth_data)

if auth_response.status_code == 200:
    token = auth_response.json()['token']
    endpoint = "http://localhost:8000/api/product/"
    headers = {"Authorization": f'Bearer {token}'}
    get_response = requests.get(endpoint, headers=headers)
    print(get_response.status_code)
    print(get_response.json())
